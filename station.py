class Station:
    """
        Represent a bus station
	
        PROPERTIES
        name (String) : Name of the bus station
        neighbours (List(Station)) : Neighbours of this station according to lines
        lines (List(Line)) : Lines that can be stopped at this station
    """
    def __init__(self,name,lines=[],neighbours=[]):
        """
            Initialization of a bus station
            name (String) : Name of the bus station
            neighbours (List(Station)) : Neighbours of this station according to lines
            lines (List(Line)) : Lines that can be stopped at this station
            RETURN void
        """
        self.name=Station.nameFormat(name)
        self.neighbours=neighbours
        self.lines=lines

    @staticmethod
    def nameFormat(value):
        """
            Clear a station name
            value (String) : the uncleared value
            RETURN (String) : the cleared value
        """
        return value.strip().lower().replace(' ','')

    @staticmethod
    def scheduleToMinute(value):
        """
            Transform a schedule to his number of minutes
            value (String) : the schedule in format HH:SS
            RETURN (Int) : the number of minutes
        """
        value=value.strip().split(':')
        return int(value[0])*60+int(value[1])

    @staticmethod
    def minuteToSchedule(value):
        """
            Transform a number of minutes to a schedule
            value (Int) : the number of minutes
            RETURN (String) : the schedule in format HH:SS
        """
        return (value/60)+':'+(value%60)

    @staticmethod
    def scheduleDifference(value1,value2):
        """
            Give the number of minutes between two schedule
            value1 (String) : the basis schedule in format HH:SS
            value2 (String) : the schedule to differenciate in format HH:SS
            RETURN (Int) : the result in number of minutes
            
            EXAMPLES : Station.scheduleDifference('9:45','9:30') returns 15, Station.scheduleDifference('9:30','9:45') returns -15
        """
        return Station.scheduleToMinute(value1)-Station.scheduleToMinute(value2)
