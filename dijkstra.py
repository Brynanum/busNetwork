import station

def dijkstra(stationStart,schedule=None):
    """
        Apply the Dijkstra's algorithm
        stationStart (Station) : the wanted beginning station
        schedule (String) : the started schedule in format HH:SS (if None is given, this will apply on physical distance)
        RETURN (List(List(Station,Unsigned int,Station))) : The result of the algorithm, in Station/distance/previousStation format
        
        WARNING : Realize many subfunctions for maintain easily the code
    """
    cout=schedule
    if(schedule==None):
        cout=0
    tracks=[[stationStart,cout,None,False]] # [[station concerne, cout, station precedente, verrouillage]]
    while(True):
        # Choix de la station
        s=next((s for s in tracks if(not s[3])),False)
        # Parcours des voisins
        for sn in s[0].neighbours:
            # Determination du cout
            if(schedule==None):
                cout=1+s[1]
            else:
                lines=[l for l in s[0].lines if l in sn.lines]
                best=None
                for line in [l for l in s[0].lines if l in sn.lines]:
                    for index in range(len(line.stations)):
                        try:
                            # line.stations[index+1][1][*][1] est le tableau des horaires en direction de *
                            if(line.stations[index][0]==s and index+1!=len(line.stations)):
                                cout=[hour for hour in line.stations[index+1][1][0][1] if station.Station.scheduleDifference(hour,s[1])>-1][0]
                                break
                            elif(line.stations[index][0]==sn and index+1!=len(line.stations)):
                                cout=[hour for hour in line.stations[index+1][1][1][1] if station.Station.scheduleDifference(hour,s[1])>-1][0]
                                break
                        except:
                            return None
            # Prise en consideration dans les chemins possible
            if(sn in [i[0] for i in tracks]):
                sn=[i[0] for i in tracks].index(sn)
                if(not(tracks[sn][3]) and tracks[sn][1]>cout):
                    tracks[sn]=[tracks[sn][0],cout,s,False]
            else:
                tracks.append([sn,cout,s[0],False])
        s[3]=True
        # Arret de la boucle : Fin de l'algorithme
        continuer=False
        for s in tracks:
            if(not s[3]):
                continuer=True
                break
        if(not continuer):
            return [t[:3] for t in tracks]

def bestTrack(stationStart,stationEnd,scheduleStart=None):
    """
        Give the best track according to the dijkstra's result
        stationStart (Station) : The beginning station
        stationEnd (Station) : The ended station
        scheduleStart (String) : The beginning schedule in format HH:SS
        RETURN List(Station)+List(Unsigned int / String) : The path of the solution + the value of the best result (in distance or schedule)
    """
    dijkstraResult=dijkstra(stationStart,scheduleStart)
    if(dijkstraResult==None):
        return None
    track=[s for s in dijkstraResult if s[0].name==stationEnd.name]
    track=[track[0][1],track[0][0]]
    while(track[-1]!=stationStart):
        track.append([s[2] for s in dijkstraResult if s[0].name==track[-1].name][0])
    return track[::-1]


def shortest(stationStart,stationEnd):
    """
        Give the shortest track between two stations in distance
        stationStart (Station) : The beginning station
        stationEnd (Station) : The ended station
        RETURN List(Station)+List(Unsigned int) : The path of the solution + the distance of the best result
    """
    return bestTrack(stationStart,stationEnd)

def fastest(stationStart,stationEnd):
    """
        Give the fastest track between two stations in time, without beginning schedule
        stationStart (Station) : The beginning station
        stationEnd (Station) : The ended station
        RETURN List(Station)+List(String) : The path of the solution + the clock of the best result
        
        WARNING : Time execution too long, can be optimize
    """
    print('This function can take a little time. Please wait...')
    # Recensement des trajets et durees possible
    schedules=[]
    for l in stationStart.lines:
        hours=[s[1] for s in l.stations if s[0].name==stationStart.name][0]
        hours=sum([h[1] for h in hours],[])
        for h in hours:
            hForemost=foremost(stationStart,stationEnd,h)
            if(hForemost!=None):
                schedules.append([h]+hForemost)
        for i in range(len(schedules)):
            schedules[i].append(station.Station.scheduleDifference(schedules[i][-1],schedules[i][0]))
    # Recherche du trajet le moins long
    delay=[s[-1] for s in schedules]
    delay=delay.index(min(delay))
    return schedules[delay]

def foremost(stationStart,stationEnd,scheduleStart):
    """
        Give the foremost track between two stations in time, according to a beginning schedule
        stationStart (Station) : The beginning station
        stationEnd (Station) : The ended station
        scheduleStart (String) : The beginning schedule in format HH:SS
        RETURN List(Station)+List(String) : The path of the solution + the clock of the best result
    """
    return bestTrack(stationStart,stationEnd,scheduleStart)
