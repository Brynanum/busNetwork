# Polytech - Bus network
## Introduction
Realize a bus network, and estimate shortest, fastest and foremost way to join 2 stations.

## How can I use it ?
Read the documentation in doc folder. You can find more explanations by executing main.py.

## Documentation
You can find all technical documentation in the "doc" folder, as HTML files.
You are able to see directly these files on your web browser : copy the link of the wanted documentation file on https://htmlpreview.github.io to see it.

## Required Python libraries
There are no required libraries.

# Author
Bryan Fauquembergue

# Improvement
This project will not have some amelioration. However, you can fork it and share your own ameliorations, while you expect the license.