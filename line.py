from re import findall,search
from station import Station
 
class Line:
    """
    	Represent a line of bus
    	
    	PROPERTIES
    	name (Unsigned int) : Name of the line
    	stations (List(Station)) : Stations 
    """
    def __init__(self,name=0,stations=[]):
        """
    	    Initialization of a line
    	    name (Unsigned int) : Name of the line
    	    stations (List(Station)) : Stations on this line (in format [[aStation(Station),[[directionTerminus(Station),[schedules(String)]]]]])
    	    RETURN void
        """
        self.name=name
        self.stations=stations

    def upload(self,fileLine):
        """
            Create line and stations according to .txt files in data folder
            fileLine (String) : the path to the wanted TXT file in data folder
            RETURN void

            WARNING : Realize many subfunctions for maintain easily the code
        """
        stations=[]
        result=[]
        with open(fileLine,'r') as fileLine:
            for read in fileLine:
                if(search('[0-9]+',read)):
                    # Recuperation de la station
                    read=read.strip().split(' ')
                    s=next((st for st in stations if st.name==Station.nameFormat(read[0])),False)
                    if(not s):
                        s=Station(read[0],[self])
                        stations.append(s)
            # Declaration des voisins de chaque station
            for i in range(1,len(stations)-1):
                stations[i].neighbours=[stations[i-1],stations[i+1]]
            stations[0].neighbours=[stations[1]]
            stations[-1].neighbours=[stations[-2]]

            fileLine.seek(0)
            terminus=''
            for read in fileLine:
                # Determination du terminus
                if(read=='\n'):
                    if(terminus=='' or terminus==stations[0]):
                        terminus=stations[-1]
                    else:
                        terminus=stations[0]
                # Ajout des horaires
                elif(search('[0-9]+',read)):
                    read=read.strip().split(' ')
                    read=list(filter(lambda r: r!='-',read))
                    for i in range(1,len(read)):
                        s=next((st for st in stations if st.name==Station.nameFormat(read[0])),False)
                        sc=next((st for st in result if st[0]==s),False)
                        if(sc):
                        	s=next((sl for sl in result if sl[0]==s),False)[1]
                        	st=next((sl for sl in s if sl[0]==terminus),False)
                        	if(st):
                        		st[1].append(read[i])
                        	else:
                        		s.append([terminus,[read[i]]])
                        else:
                        	result.append([s,[[terminus,[read[i]]]]])
        fileLine.close()
        self.stations=result

    def fusionate(self,line):
        """
            Associate common stations between two lines
            line (Line) : the other line to associate
            RETURN void
        """
        for ss in self.stations:
            for sl in line.stations:
                if(sorted(ss[0].name)==sorted(sl[0].name)):
                    for l in sl[0].lines:
                        ss[0].lines.append(l)
                    for n in sl[0].neighbours:
                        ss[0].neighbours.append(n)
                    sl[0]=ss[0]
