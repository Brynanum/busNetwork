import line as l
import station as s
import dijkstra as d

# Data initialization
l1=l.Line(1)
l2=l.Line(2)
l1.upload('data/1_Poisy-ParcDesGlaisins.txt')
l2.upload('data/2_Piscine-Patinoire_Campus.txt')
l1.fusionate(l2)

# Data structure
print('Data structure (please read the documentation of line.py) :')
print(l1.stations[2])
print('')

# Algorithms
print('Algorithm - Shortest way :')
print(d.shortest(l1.stations[2][0],l2.stations[7][0]))
print('')
print('Algorithm - Foremost way :')
print(d.foremost(l1.stations[2][0],l2.stations[7][0],'9:30'))
print('')
print('Algorithm - Fastest way :')
print(d.fastest(l1.stations[2][0],l2.stations[7][0]))
